package com.atguigu.ssyx.log.aop;

import com.atguigu.ssyx.log.annotations.Log;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Aspect
@Component
@Slf4j
public class LogAspect {

    @Around("@annotation(com.atguigu.ssyx.log.annotations.Log)")
    public Object doLog(ProceedingJoinPoint joinPoint) {
        Object object;
        //FileWriter fileWriter;
        try {
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();
            Log sysLog = signature.getMethod().getAnnotation(Log.class);
            String name = signature.getName();
            String logValue = sysLog.logValue();
            object = joinPoint.proceed();
            String logText = new StringBuilder()
                    .append("方法: ")
                    .append(name)
                    .append("时间: ")
                    .append(LocalDateTime.now())
                    .append("内容: ")
                    .append(logValue)
                    .toString();
            log.info(logText);
            return object;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }
}
