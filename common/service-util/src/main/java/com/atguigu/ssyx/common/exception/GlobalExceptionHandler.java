package com.atguigu.ssyx.common.exception;

import com.atguigu.ssyx.common.Result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 统一业务异常处理
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public Result exceptionHandler(Exception e) {
        log.info("异常信息：{}", e.getMessage());
        return Result.fail(e.getMessage());
    }

    @ExceptionHandler(CustomException.class)
    public Result exceptionHandler(CustomException e) {
        log.info("异常信息：{}", e.getMessage());
        return Result.build(null, e.getCode(), e.getMessage());
    }
}
