package com.atguigu.ssyx.common.exception;

import com.atguigu.ssyx.common.Result.ResultCodeEnum;
import lombok.Data;

@Data
public class CustomException extends RuntimeException {

    private Integer code;

    public CustomException(String message, Integer code) {
        super(message);
        this.code = code;
    }

    public CustomException(ResultCodeEnum resultCodeEnum) {
        super(resultCodeEnum.getMessage());
        this.code = resultCodeEnum.getCode();
    }
}
