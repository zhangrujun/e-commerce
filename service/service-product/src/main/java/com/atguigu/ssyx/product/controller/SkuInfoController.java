package com.atguigu.ssyx.product.controller;


import com.atguigu.ssyx.common.Result.Result;
import com.atguigu.ssyx.model.product.SkuInfo;
import com.atguigu.ssyx.mq.constant.MqConstant;
import com.atguigu.ssyx.mq.service.RabbitService;
import com.atguigu.ssyx.product.service.SkuInfoService;
import com.atguigu.ssyx.vo.product.SkuInfoQueryVo;
import com.atguigu.ssyx.vo.product.SkuInfoVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * sku信息 前端控制器
 * </p>
 *
 * @author L0VE、SsJuN
 * @since 2023-10-01
 */
@Api(tags = "商品Sku管理")
@RestController
@RequestMapping(value = "/admin/product/skuInfo")
public class SkuInfoController {

    @Autowired
    private SkuInfoService skuInfoService;

    @Autowired
    private RabbitService rabbitService;

    @ApiOperation("获取sku分页列表")
    @GetMapping("/{page}/{limit}")
    public Result<IPage<SkuInfo>> list(
            @PathVariable Long page,
            @PathVariable Long limit,
            SkuInfoQueryVo skuInfoQueryVo) {
        Page<SkuInfo> pageParam = new Page<>(page, limit);
        IPage<SkuInfo> result = skuInfoService.selectPage(pageParam, skuInfoQueryVo);
        return Result.ok(result);
    }

    //商品添加方法
    @ApiOperation("新增")
    @PostMapping("/save")
    public Result save(@RequestBody SkuInfoVo skuInfoVo) {
        skuInfoService.saveSkuInfo(skuInfoVo);
        return Result.ok();
    }

    @ApiOperation("修改")
    @PutMapping("/update")
    public Result update(@RequestBody SkuInfoVo skuInfoVo) {
        skuInfoService.updateSkuInfo(skuInfoVo);
        return Result.ok();
    }

    @ApiOperation("根据skuId查询")
    @GetMapping("/get/{id}")
    public Result<SkuInfoVo> getBySkuId(@PathVariable Long id) {
        SkuInfoVo skuInfoVo = skuInfoService.selectBySkuId(id);
        return Result.ok(skuInfoVo);
    }

    @ApiOperation("删除")
    @DeleteMapping("/remove/{id}")
    public Result remove(@PathVariable Long id) {
        skuInfoService.removeById(id);
        return Result.ok();
    }

    @ApiOperation("根据id列表删除")
    @DeleteMapping("/batchRemove")
    public Result batchRemove(@RequestBody List<Long> idList) {
        skuInfoService.removeBySkuIds(idList);
        return Result.ok();
    }

    /**
     * 商品审核
     *
     * @param skuId
     * @return
     */
    @ApiOperation("商品审核")
    @GetMapping("/check/{skuId}/{status}")
    public Result check(@PathVariable Long skuId, @PathVariable Integer status) {
        skuInfoService.check(skuId, status);
        return Result.ok();
    }

    /**
     * 商品上架
     *
     * @param skuId
     * @return
     */
    @ApiOperation("商品上架")
    @GetMapping("/publish/{skuId}/{status}")
    public Result publish(@PathVariable Long skuId, @PathVariable Integer status) {
        skuInfoService.publish(skuId, status);
        return Result.ok();
    }

    @ApiOperation("新人专享")
    @GetMapping("/isNewPerson/{skuId}/{status}")
    public Result isNewPerson(@PathVariable Long skuId,
                              @PathVariable Integer status) {
        skuInfoService.isNewPerson(skuId, status);
        return Result.ok();
    }
}

