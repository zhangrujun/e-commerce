package com.atguigu.ssyx.product.service;

import com.atguigu.ssyx.model.product.SkuAttrValue;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * spu属性值 服务类
 * </p>
 *
 * @author L0VE、SsJuN
 * @since 2023-10-01
 */
public interface SkuAttrValueService extends IService<SkuAttrValue> {

    List<SkuAttrValue> selectBySkuId(Long skuId);

    void saveSkuAttrValueList(Long skuId, List<SkuAttrValue> skuAttrValueList);
}
