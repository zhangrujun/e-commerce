package com.atguigu.ssyx.product.service;

import com.atguigu.ssyx.model.product.SkuPoster;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 商品海报表 服务类
 * </p>
 *
 * @author L0VE、SsJuN
 * @since 2023-10-01
 */
public interface SkuPosterService extends IService<SkuPoster> {

    List<SkuPoster> selectBySkuId(Long skuId);

    void saveSkuPosterList(Long skuId, List<SkuPoster> skuPosterList);
}
