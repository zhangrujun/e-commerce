package com.atguigu.ssyx.product.mapper;

import com.atguigu.ssyx.model.product.SkuAttrValue;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * spu属性值 Mapper 接口
 * </p>
 *
 * @author L0VE、SsJuN
 * @since 2023-10-01
 */
@Mapper
public interface SkuAttrValueMapper extends BaseMapper<SkuAttrValue> {

}
