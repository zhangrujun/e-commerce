package com.atguigu.ssyx.product.controller;


import com.atguigu.ssyx.common.Result.Result;
import com.atguigu.ssyx.log.annotations.Log;
import com.atguigu.ssyx.model.product.Attr;
import com.atguigu.ssyx.product.service.AttrService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 商品属性 前端控制器
 * </p>
 *
 * @author L0VE、SsJuN
 * @since 2023-10-01
 */
@Api(tags = "平台属性管理")
@RestController
@RequestMapping(value = "/admin/product/attr")
public class AttrController {

    @Autowired
    private AttrService attrService;

    @ApiOperation(value = "获取列表")
    @GetMapping("/{attrGroupId}")
    @Log(logValue = "测试一下aop看看能不能用")
    public Result<List<Attr>> list(@PathVariable Long attrGroupId) {
        List<Attr> result = attrService.findByAttrGroupId(attrGroupId);
        return Result.ok(result);
    }

    @ApiOperation(value = "获取")
    @GetMapping("/get/{id}")
    public Result<Attr> get(@PathVariable Long id) {
        Attr attr = attrService.getById(id);
        return Result.ok(attr);
    }

    @ApiOperation(value = "新增")
    @PostMapping("/save")
    public Result save(@RequestBody Attr attr) {
        attrService.save(attr);
        return Result.ok();
    }

    @ApiOperation(value = "修改")
    @PutMapping("/update")
    public Result updateById(@RequestBody Attr attr) {
        attrService.updateById(attr);
        return Result.ok();
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/remove/{id}")
    public Result remove(@PathVariable Long id) {
        attrService.removeById(id);
        return Result.ok();
    }

    @ApiOperation(value = "根据id列表删除")
    @DeleteMapping("/batchRemove")
    public Result batchRemove(@RequestBody List<Long> idList) {
        attrService.removeByIds(idList);
        return Result.ok();
    }
}

