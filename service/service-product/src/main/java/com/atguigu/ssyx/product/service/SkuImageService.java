package com.atguigu.ssyx.product.service;

import com.atguigu.ssyx.model.product.SkuImage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 商品图片 服务类
 * </p>
 *
 * @author L0VE、SsJuN
 * @since 2023-10-01
 */
public interface SkuImageService extends IService<SkuImage> {

    List<SkuImage> selectBySkuId(Long skuId);

    void saveSkuImageList(Long skuId, List<SkuImage> skuImagesList);
}
