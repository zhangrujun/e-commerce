package com.atguigu.ssyx.product.service.impl;

import com.alibaba.nacos.client.naming.utils.CollectionUtils;
import com.atguigu.ssyx.model.product.SkuImage;
import com.atguigu.ssyx.product.mapper.SkuImageMapper;
import com.atguigu.ssyx.product.service.SkuImageService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 商品图片 服务实现类
 * </p>
 *
 * @author L0VE、SsJuN
 * @since 2023-10-01
 */
@Service
public class SkuImageServiceImpl extends ServiceImpl<SkuImageMapper, SkuImage> implements SkuImageService {

    @Autowired
    private SkuImageService skuImageService;

    @Override
    public List<SkuImage> selectBySkuId(Long skuId) {
        LambdaQueryWrapper<SkuImage> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SkuImage::getSkuId, skuId);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public void saveSkuImageList(Long skuId, List<SkuImage> skuImagesList) {
        if (!CollectionUtils.isEmpty(skuImagesList)) {
            for (SkuImage skuImages : skuImagesList) {
                skuImages.setSkuId(skuId);
            }
            skuImageService.saveBatch(skuImagesList);
        }
    }
}
