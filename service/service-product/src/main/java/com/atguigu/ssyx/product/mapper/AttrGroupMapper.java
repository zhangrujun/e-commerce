package com.atguigu.ssyx.product.mapper;


import com.atguigu.ssyx.model.product.AttrGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 属性分组 Mapper 接口
 * </p>
 *
 * @author L0VE、SsJuN
 * @since 2023-10-01
 */
@Mapper
public interface AttrGroupMapper extends BaseMapper<AttrGroup> {

}
