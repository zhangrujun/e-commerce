package com.atguigu.ssyx.product.service.impl;

import com.atguigu.ssyx.common.Result.ResultCodeEnum;
import com.atguigu.ssyx.common.constant.RedisConstant;
import com.atguigu.ssyx.common.exception.CustomException;
import com.atguigu.ssyx.model.product.SkuAttrValue;
import com.atguigu.ssyx.model.product.SkuImage;
import com.atguigu.ssyx.model.product.SkuInfo;
import com.atguigu.ssyx.model.product.SkuPoster;
import com.atguigu.ssyx.mq.constant.MqConstant;
import com.atguigu.ssyx.mq.service.RabbitService;
import com.atguigu.ssyx.product.mapper.SkuInfoMapper;
import com.atguigu.ssyx.product.service.SkuAttrValueService;
import com.atguigu.ssyx.product.service.SkuImageService;
import com.atguigu.ssyx.product.service.SkuInfoService;
import com.atguigu.ssyx.product.service.SkuPosterService;
import com.atguigu.ssyx.vo.product.SkuInfoQueryVo;
import com.atguigu.ssyx.vo.product.SkuInfoVo;
import com.atguigu.ssyx.vo.product.SkuStockLockVo;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 * sku信息 服务实现类
 * </p>
 *
 * @author L0VE、SsJuN
 * @since 2023-10-01
 */
@Service
public class SkuInfoServiceImpl extends ServiceImpl<SkuInfoMapper, SkuInfo> implements SkuInfoService {

    @Autowired
    private SkuPosterService skuPosterService;

    @Autowired
    private SkuImageService skuImagesService;

    @Autowired
    private SkuInfoMapper skuInfoMapper;

    @Autowired
    private SkuAttrValueService skuAttrValueService;

    @Autowired
    private RabbitService rabbitService;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private RedisTemplate redisTemplate;

    //添加商品
    @Transactional(rollbackFor = {Exception.class})
    @Override
    public void saveSkuInfo(SkuInfoVo skuInfoVo) {
        //保存sku信息
        SkuInfo skuInfo = new SkuInfo();
        BeanUtils.copyProperties(skuInfoVo, skuInfo);
        //插入sku信息
        baseMapper.insert(skuInfo);
        //获取插入的skuId
        Long skuId = skuInfo.getId();
        //保存sku海报
        skuPosterService.saveSkuPosterList(skuId, skuInfoVo.getSkuPosterList());
        //保存sku图片
        skuImagesService.saveSkuImageList(skuId, skuInfoVo.getSkuImagesList());
        //保存sku平台属性
        skuAttrValueService.saveSkuAttrValueList(skuId, skuInfoVo.getSkuAttrValueList());
    }

    @Override
    public SkuInfoVo selectBySkuId(Long id) {
        //根据id查询
        SkuInfo skuInfo = baseMapper.selectById(id);
        //创建vo对象
        SkuInfoVo skuInfoVo = new SkuInfoVo();
        //复制属性
        BeanUtils.copyProperties(skuInfo, skuInfoVo);
        //查询海报列表
        List<SkuPoster> skuPosterList = skuPosterService.selectBySkuId(id);
        //查询sku属性值列表
        List<SkuAttrValue> skuAttrValueList = skuAttrValueService.selectBySkuId(id);
        //查询图片列表
        List<SkuImage> skuImageList = skuImagesService.selectBySkuId(id);
        //设置值
        skuInfoVo.setSkuPosterList(skuPosterList);
        skuInfoVo.setSkuAttrValueList(skuAttrValueList);
        skuInfoVo.setSkuImagesList(skuImageList);
        return skuInfoVo;
    }

    @Transactional(rollbackFor = {Exception.class})
    @Override
    public void updateSkuInfo(SkuInfoVo skuInfoVo) {
        //创建skuInfo对象
        SkuInfo skuInfo = new SkuInfo();
        //复制属性
        BeanUtils.copyProperties(skuInfoVo, skuInfo);
        //修改
        baseMapper.updateById(skuInfo);
        //获取skuId
        Long skuId = skuInfo.getId();
        //根据skuId删除原本已存在的海报
        skuPosterService.remove(new LambdaQueryWrapper<SkuPoster>().eq(SkuPoster::getSkuId, skuId));
        //重新插入海报
        skuPosterService.saveSkuPosterList(skuId, skuInfoVo.getSkuPosterList());
        //根据skuId删除原本已存在的图片
        skuImagesService.remove(new LambdaQueryWrapper<SkuImage>().eq(SkuImage::getSkuId, skuId));
        //重新插入图片
        skuImagesService.saveSkuImageList(skuId, skuInfoVo.getSkuImagesList());
        //根据skuId删除原本已存在的属性值
        skuAttrValueService.remove(new LambdaQueryWrapper<SkuAttrValue>().eq(SkuAttrValue::getSkuId, skuId));
        //重新插入属性信息
        skuAttrValueService.saveSkuAttrValueList(skuId, skuInfoVo.getSkuAttrValueList());
    }

    @Override
    public void check(Long skuId, Integer status) {
        // 更改发布状态
        SkuInfo skuInfoUp = new SkuInfo();
        skuInfoUp.setId(skuId);
        skuInfoUp.setCheckStatus(status);
        baseMapper.updateById(skuInfoUp);
    }

    //商品上架
    @Transactional(rollbackFor = {Exception.class})
    @Override
    public void publish(Long skuId, Integer status) {
        SkuInfo skuInfoUp = new SkuInfo();
        skuInfoUp.setId(skuId);
        // 更改发布状态
        if (status == 1) {
            skuInfoUp.setPublishStatus(1);
            //调用rabbitMQ更新上架信息
            rabbitService.sendMessage(MqConstant.EXCHANGE_GOODS_DIRECT, MqConstant.ROUTING_GOODS_UPPER, skuId);
        } else {
            skuInfoUp.setPublishStatus(0);
            //调用rabbitMQ更新下架信息
            rabbitService.sendMessage(MqConstant.EXCHANGE_GOODS_DIRECT, MqConstant.ROUTING_GOODS_LOWER, skuId);
        }

        baseMapper.updateById(skuInfoUp);
    }

    @Override
    public List<SkuInfo> findSkuInfoByKeyword(String keyword) {
        LambdaQueryWrapper<SkuInfo> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StringUtils.isNotEmpty(keyword), SkuInfo::getSkuName, keyword);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public void isNewPerson(Long skuId, Integer status) {
        SkuInfo skuInfoUp = new SkuInfo();
        skuInfoUp.setId(skuId);
        skuInfoUp.setIsNewPerson(status);
        skuInfoMapper.updateById(skuInfoUp);
    }

    @Override
    public List<SkuInfo> findNewPersonList() {
        IPage<SkuInfo> page = new Page<>(1, 3);
        LambdaQueryWrapper<SkuInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SkuInfo::getIsNewPerson, 1)
                .eq(SkuInfo::getPublishStatus, 1)
                .orderByDesc(SkuInfo::getStock);
        return skuInfoMapper.selectPage(page, queryWrapper)
                .getRecords();
    }

    @Override
    public void removeBySkuIds(List<Long> idList) {
        idList.forEach(id -> {
            baseMapper.deleteById(id);
            rabbitService.sendMessage(MqConstant.EXCHANGE_GOODS_DIRECT, MqConstant.ROUTING_GOODS_LOWER, id);
        });
    }

    @Override
    public void minusStock(String orderNo) {
        // 获取锁定库存的缓存信息
        List<SkuStockLockVo> skuStockLockVoList = (List<SkuStockLockVo>)this.redisTemplate.opsForValue().get(RedisConstant.SROCK_INFO + orderNo);
        if (CollectionUtils.isEmpty(skuStockLockVoList)){
            return;
        }

        // 减库存
        skuStockLockVoList.forEach(skuStockLockVo -> {
            skuInfoMapper.minusStock(skuStockLockVo.getSkuId(), skuStockLockVo.getSkuNum());
        });

        // 解锁库存之后，删除锁定库存的缓存。以防止重复解锁库存
        this.redisTemplate.delete(RedisConstant.SROCK_INFO + orderNo);
    }

    @Override
    public Boolean checkAndLock(List<SkuStockLockVo> skuStockLockVoList, String orderNo) {
        //判断商品信息是否为空
        if (CollectionUtils.isEmpty(skuStockLockVoList)) {
            throw new CustomException(ResultCodeEnum.DATA_ERROR);
        }
        //锁定库存
        skuStockLockVoList.forEach(this::checkLock);
        //判断是否有锁定失败的选项
        boolean flag = skuStockLockVoList.stream().anyMatch(skuStockLockVo -> !skuStockLockVo.getIsLock());
        if (flag) {
            // 获取所有锁定成功的商品，遍历解锁库存
            skuStockLockVoList.stream().filter(SkuStockLockVo::getIsLock).forEach(skuStockLockVo -> {
                skuInfoMapper.unlockStock(skuStockLockVo.getSkuId(), skuStockLockVo.getSkuNum());
            });
            return false;
        }
        redisTemplate.opsForValue().set(RedisConstant.SROCK_INFO + orderNo, skuStockLockVoList);
        return true;
    }

    private void checkLock(SkuStockLockVo skuStockLockVo) {
        //公平锁，就是保证客户端获取锁的顺序，跟他们请求获取锁的顺序，是一样的。
        // 公平锁需要排队
        // ，谁先申请获取这把锁，
        // 谁就可以先获取到这把锁，是按照请求的先后顺序来的。
        RLock rLock = this.redissonClient
                .getFairLock(RedisConstant.SKU_KEY_PREFIX + skuStockLockVo.getSkuId());
        rLock.lock();

        try {
            // 验库存：查询，返回的是满足要求的库存列表
            SkuInfo skuInfo = skuInfoMapper.checkStock(skuStockLockVo.getSkuId(), skuStockLockVo.getSkuNum());
            // 如果没有一个仓库满足要求，这里就验库存失败
            if (null == skuInfo) {
                skuStockLockVo.setIsLock(false);
                return;
            }

            // 锁库存：更新
            Integer row = skuInfoMapper.lockStock(skuStockLockVo.getSkuId(), skuStockLockVo.getSkuNum());
            if (row == 1) {
                skuStockLockVo.setIsLock(true);
            }
        } finally {
            rLock.unlock();
        }
    }

    @Override
    public IPage<SkuInfo> selectPage(Page<SkuInfo> pageParam, SkuInfoQueryVo skuInfoQueryVo) {
        //获取条件值
        String keyword = skuInfoQueryVo.getKeyword();
        String skuType = skuInfoQueryVo.getSkuType();
        Long categoryId = skuInfoQueryVo.getCategoryId();
        //封装条件
        LambdaQueryWrapper<SkuInfo> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StringUtils.isNotEmpty(keyword), SkuInfo::getSkuName, keyword)
                .eq(StringUtils.isNotEmpty(keyword), SkuInfo::getSkuType, skuType)
                .eq(Objects.nonNull(categoryId), SkuInfo::getCategoryId, categoryId);
        //调用方法查询
        return baseMapper.selectPage(pageParam, wrapper);
    }
}
