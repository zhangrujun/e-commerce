package com.atguigu.ssyx.product.controller;


import com.atguigu.ssyx.common.Result.Result;
import com.atguigu.ssyx.model.product.Category;
import com.atguigu.ssyx.product.service.CategoryService;
import com.atguigu.ssyx.vo.product.CategoryQueryVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 商品三级分类 前端控制器
 * </p>
 *
 * @author atguigu
 * @since 2023-09-30
 */
@Api(tags = "商品分类相关接口")
@RestController
@RequestMapping("/admin/product/category")
@Slf4j
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     * 查询商品分类信息
     *
     * @return
     */
    @ApiOperation("查询商品分类信息")
    @GetMapping("/{page}/{limit}")
    public Result<IPage<Category>> list(@PathVariable Long page,
                                        @PathVariable Long limit,
                                        CategoryQueryVo categoryQueryVo) {
        Page<Category> pageParam = new Page<>(page, limit);
        log.info("分页条件: {},{}", pageParam.getCurrent(), pageParam.getSize());
        IPage<Category> result = categoryService.selectPageCategory(pageParam, categoryQueryVo);
        log.info("结果:{}, {}, {}", result.getTotal(), result.getSize(), result.getCurrent());
        return Result.ok(result);
    }

    /**
     * 查询所有商品分类列表
     *
     * @return
     */
    @ApiOperation("查询所有商品分类列表")
    @GetMapping("/findAllList")
    public Result<List<Category>> findAllList() {
        List<Category> list = categoryService.list();
        return Result.ok(list);
    }

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    @ApiOperation("根据id查询")
    @GetMapping("/get/{id}")
    public Result<Category> getById(@PathVariable Long id) {
        Category category = categoryService.getById(id);
        return Result.ok(category);
    }

    /**
     * 新增商品分类
     *
     * @param category
     * @return
     */
    @ApiOperation("新增商品分类")
    @PostMapping("/save")
    public Result save(@RequestBody Category category) {
        return categoryService.save(category) ? Result.ok() : Result.fail();
    }

    /**
     * 修改商品分类
     *
     * @param category
     * @return
     */
    @ApiOperation("修改商品分类")
    @PutMapping("/update")
    public Result updateById(@RequestBody Category category) {
        return categoryService.updateById(category) ? Result.ok() : Result.fail();
    }

    /**
     * 删除商品分类
     *
     * @param id
     * @return
     */
    @ApiOperation("删除商品分类")
    @DeleteMapping("/remove/{id}")
    public Result remove(@PathVariable Long id) {
        categoryService.removeById(id);
        return Result.ok();
    }

    /**
     * 批量删除商品分类
     *
     * @param idList
     * @return
     */
    @ApiOperation("根据id列表删除商品分类")
    @DeleteMapping("/batchRemove")
    public Result batchRemove(@RequestBody List<Long> idList) {
        categoryService.removeByIds(idList);
        return Result.ok();
    }
}

