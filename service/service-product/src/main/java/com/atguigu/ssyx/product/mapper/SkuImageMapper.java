package com.atguigu.ssyx.product.mapper;

import com.atguigu.ssyx.model.product.SkuImage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 商品图片 Mapper 接口
 * </p>
 *
 * @author L0VE、SsJuN
 * @since 2023-10-01
 */
@Mapper
public interface SkuImageMapper extends BaseMapper<SkuImage> {

}
