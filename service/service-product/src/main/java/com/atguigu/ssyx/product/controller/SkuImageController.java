package com.atguigu.ssyx.product.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 商品图片 前端控制器
 * </p>
 *
 * @author L0VE、SsJuN
 * @since 2023-10-01
 */
@RestController
@RequestMapping("/product/sku-image")
public class SkuImageController {

}

