package com.atguigu.ssyx.product.mapper;

import com.atguigu.ssyx.model.product.SkuPoster;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 商品海报表 Mapper 接口
 * </p>
 *
 * @author L0VE、SsJuN
 * @since 2023-10-01
 */
@Mapper
public interface SkuPosterMapper extends BaseMapper<SkuPoster> {

}
