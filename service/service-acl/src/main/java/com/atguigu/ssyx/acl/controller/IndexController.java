package com.atguigu.ssyx.acl.controller;

import com.atguigu.ssyx.common.Result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Api(tags = "登录相关接口")
@RestController
@RequestMapping("/admin/acl/index")
public class IndexController {

    /**
     * 登录接口
     *
     * @return Result
     */
    @ApiOperation("登录接口")
    @PostMapping("/login")
    public Result login() {
        Map<String, String> map = new HashMap<>();
        map.put("token", "token-admin");
        return Result.ok(map);
    }

    /**
     * 获取用户信息
     *
     * @return Result
     */
    @ApiOperation("获取信息")
    @GetMapping("/info")
    public Result getInfo() {
        Map<String, Object> map = new HashMap<>();
        map.put("name", "你爸爸");
        map.put("avatar", "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
        return Result.ok(map);
    }

    /**
     * 推出登录
     *
     * @return Result
     */
    @ApiOperation("退出登录")
    @PostMapping("/logout")
    public Result logOut() {
        return Result.ok(null);
    }
}
