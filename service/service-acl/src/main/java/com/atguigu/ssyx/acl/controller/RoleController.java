package com.atguigu.ssyx.acl.controller;

import com.atguigu.ssyx.acl.service.RoleService;
import com.atguigu.ssyx.common.Result.Result;
import com.atguigu.ssyx.model.acl.Role;
import com.atguigu.ssyx.vo.acl.RoleQueryVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "角色管理接口")
@RestController
@RequestMapping("/admin/acl/role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    /**
     * 分页查询
     *
     * @param current
     * @param limit
     * @param roleQueryVo
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/{current}/{limit}")
    public Result<IPage<Role>> pageList(@PathVariable Long current,
                                        @PathVariable Long limit,
                                        RoleQueryVo roleQueryVo) {
        Page<Role> page = new Page<>(current, limit);
        IPage<Role> pageModel = roleService.selectRolePage(page, roleQueryVo);
        return Result.ok(pageModel);
    }

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    @ApiOperation("根据id查询")
    @GetMapping("/get/{id}")
    public Result<Role> getById(@PathVariable Long id) {
        Role role = roleService.getById(id);
        return Result.ok(role);
    }

    /**
     * 新增权限
     *
     * @param role
     * @return
     */
    @ApiOperation("新增权限")
    @PostMapping("/save")
    public Result save(@RequestBody Role role) {
        return roleService.save(role) ? Result.ok() : Result.fail();
    }

    /**
     * 修改权限
     *
     * @param role
     * @return
     */
    @ApiOperation("修改权限")
    @PutMapping("/update")
    public Result update(@RequestBody Role role) {
        return roleService.updateById(role) ? Result.ok() : Result.fail();
    }

    /**
     * 根据id删除
     *
     * @param id
     * @return
     */
    @ApiOperation("根据id删除")
    @DeleteMapping("/remove/{id}")
    public Result remove(@PathVariable Long id) {
        roleService.removeById(id);
        return Result.ok();
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @ApiOperation("批量删除")
    @DeleteMapping("/batchRemove")
    public Result batchRemove(@RequestBody List<Long> ids) {
        roleService.removeByIds(ids);
        return Result.ok();
    }
}
