package com.atguigu.ssyx.acl.service;

import com.atguigu.ssyx.model.acl.Permission;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface PermissionService extends IService<Permission> {

    /**
     * 查询所有菜单
     *
     * @return
     */
    List<Permission> queryAllPermission();

    /**
     * 递归删除菜单
     *
     * @param id
     */
    void removeChildById(Long id);

    /**
     * 根据角色id查询分配的菜单
     *
     * @param roleId
     * @return
     */
    List<Permission> findByRoleId(Long roleId);

    /**
     * 保存角色和对应的
     * @param roleId
     * @param permissionId
     */
    void saveRoleAndPermission(Long roleId, Long[] permissionId);
}
