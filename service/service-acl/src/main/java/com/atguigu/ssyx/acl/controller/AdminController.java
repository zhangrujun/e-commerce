package com.atguigu.ssyx.acl.controller;

import com.atguigu.ssyx.acl.service.AdminService;
import com.atguigu.ssyx.acl.service.RoleService;
import com.atguigu.ssyx.common.Result.Result;
import com.atguigu.ssyx.model.acl.Admin;
import com.atguigu.ssyx.vo.acl.AdminQueryVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Api(tags = "用户管理接口")
@RestController
@RequestMapping("/admin/acl/user")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @Autowired
    private RoleService roleService;

    /**
     * 分页查询
     *
     * @param current
     * @param limit
     * @param adminQueryVo
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/{current}/{limit}")
    public Result<IPage<Admin>> pageList(@PathVariable Long current,
                                         @PathVariable Long limit,
                                         AdminQueryVo adminQueryVo) {
        Page<Admin> page = new Page<>(current, limit);
        IPage<Admin> pageModel = adminService.selectUserPage(page, adminQueryVo);
        return Result.ok(pageModel);
    }

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    @ApiOperation("根据id查询")
    @GetMapping("/get{id}")
    public Result<Admin> getById(@PathVariable Long id) {
        Admin admin = adminService.getById(id);
        return Result.ok(admin);
    }

    /**
     * 新增用户
     *
     * @param admin
     * @return
     */
    @ApiOperation("新增用户")
    @PostMapping("/save")
    public Result save(@RequestBody Admin admin) {
        return adminService.save(admin) ? Result.ok() : Result.fail();
    }

    /**
     * 修改用户
     *
     * @param admin
     * @return
     */
    @ApiOperation("修改用户")
    @PutMapping("/update")
    public Result update(@RequestBody Admin admin) {
        return adminService.updateById(admin) ? Result.ok() : Result.fail();
    }

    /**
     * 根据id删除
     *
     * @param id
     * @return
     */
    @ApiOperation("根据id删除")
    @DeleteMapping("/remove/{id}")
    public Result removeById(@PathVariable Long id) {
        adminService.removeById(id);
        return Result.ok(id);
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @ApiOperation("批量删除")
    @DeleteMapping("/batchRemove")
    public Result batchRemove(@RequestBody List<Long> ids) {
        adminService.removeByIds(ids);
        return Result.ok();
    }

    /**
     * 根据用户获取角色数据
     *
     * @param adminId
     * @return
     */
    @ApiOperation(value = "根据用户获取角色数据")
    @GetMapping("/toAssign/{adminId}")
    public Result<Map<String, Object>> toAssign(@PathVariable Long adminId) {
        Map<String, Object> roleMap = roleService.findRoleByUserId(adminId);
        return Result.ok(roleMap);
    }

    /**
     * 根据用户分配角色
     *
     * @param adminId
     * @param roleId
     * @return
     */
    @ApiOperation(value = "根据用户分配角色")
    @PostMapping("/doAssign")
    public Result doAssign(@RequestParam Long adminId, @RequestParam Long[] roleId) {
        roleService.saveUserRoleRelationShip(adminId, roleId);
        return Result.ok();
    }
}
