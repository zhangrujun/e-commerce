package com.atguigu.ssyx.acl.controller;

import com.atguigu.ssyx.acl.service.PermissionService;
import com.atguigu.ssyx.common.Result.Result;
import com.atguigu.ssyx.model.acl.Permission;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "菜单相关接口")
@RestController
@RequestMapping("/admin/acl/permission")
public class PermissionController {

    @Autowired
    private PermissionService permissionService;

    /**
     * 查询所有菜单
     *
     * @return
     */
    @ApiOperation(value = "获取菜单")
    @GetMapping
    public Result<List<Permission>> list() {
        List<Permission> permissionList = permissionService.queryAllPermission();
        return Result.ok(permissionList);
    }

    /**
     * 新增菜单
     *
     * @param permission
     * @return
     */
    @ApiOperation(value = "新增菜单")
    @PostMapping("/save")
    public Result save(@RequestBody Permission permission) {
        permissionService.save(permission);
        return Result.ok();
    }

    /**
     * 修改菜单
     *
     * @param permission
     * @return
     */
    @ApiOperation(value = "修改菜单")
    @PutMapping("/update")
    public Result updateById(@RequestBody Permission permission) {
        permissionService.updateById(permission);
        return Result.ok();
    }

    /**
     * 删除菜单
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "递归删除菜单")
    @DeleteMapping("/remove/{id}")
    public Result remove(@PathVariable Long id) {
        permissionService.removeChildById(id);
        return Result.ok();
    }

    /**
     * 查询菜单列表和已分配的菜单
     *
     * @param roleId
     * @return
     */
    @ApiOperation("查询菜单列表和已分配的菜单")
    @GetMapping("/toAssign/{roleId}")
    public Result<List<Permission>> toAssign(@PathVariable Long roleId) {
        List<Permission> result = permissionService.findByRoleId(roleId);
        return Result.ok(result);
    }

    /**
     * 为角色分配菜单
     *
     * @param roleId
     * @param permissionId
     * @return
     */
    @ApiOperation("为角色分配菜单")
    @PostMapping("/doAssign")
    public Result doAssign(@RequestParam Long roleId, @RequestParam Long[] permissionId) {
        permissionService.saveRoleAndPermission(roleId, permissionId);
        return Result.ok();
    }
}
