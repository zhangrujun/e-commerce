package com.atguigu.ssyx.acl.service;

import com.atguigu.ssyx.model.acl.Role;
import com.atguigu.ssyx.vo.acl.RoleQueryVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

public interface RoleService extends IService<Role> {

    /**
     * 分页查询
     *
     * @param page
     * @param roleQueryVo
     * @return
     */
    IPage<Role> selectRolePage(Page<Role> page, RoleQueryVo roleQueryVo);

    /**
     * 根据角色id查找
     *
     * @param adminId
     * @return
     */
    Map<String, Object> findRoleByUserId(Long adminId);

    /**
     * 分配角色
     *
     * @param adminId
     * @param roleId
     */
    void saveUserRoleRelationShip(Long adminId, Long[] roleId);
}
