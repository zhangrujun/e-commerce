package com.atguigu.ssyx.acl.utils;

import com.atguigu.ssyx.model.acl.Permission;

import java.util.ArrayList;
import java.util.List;

public class PermissionHelper {

    public static List<Permission> buildPermission(List<Permission> allPermissionList) {
        //创建最终集合
        List<Permission> trees = new ArrayList<>();
        for (Permission permission : allPermissionList) {
            //顶层数据
            if (permission.getPid() == 0) {
                //第一层
                permission.setLevel(1);
                //一层一层找
                trees.add(findChildren(permission, allPermissionList));
            }
        }
        //System.out.println("结果:" + trees + "长度：" + trees.size());
        return trees;
    }

    private static Permission findChildren(Permission permission, List<Permission> allPermissionList) {

        permission.setChildren(new ArrayList<>());

        //递归return后会继续下一次循环
        for (Permission p : allPermissionList) {
            //判断当前节点的id是否和pid一致
            if (permission.getId().equals(p.getPid())) {
                //下一层
                int level = permission.getLevel() + 1;
                p.setLevel(level);
                //递归封装下一层
                permission.getChildren().add(findChildren(p, allPermissionList));
            }
        }

        return permission;
    }
}
