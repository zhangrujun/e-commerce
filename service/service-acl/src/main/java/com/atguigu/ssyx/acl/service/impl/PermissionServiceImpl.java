package com.atguigu.ssyx.acl.service.impl;

import com.atguigu.ssyx.acl.mapper.PermissionMapper;
import com.atguigu.ssyx.acl.service.PermissionService;
import com.atguigu.ssyx.acl.service.RolePermissionService;
import com.atguigu.ssyx.acl.utils.PermissionHelper;
import com.atguigu.ssyx.model.acl.Permission;
import com.atguigu.ssyx.model.acl.RolePermission;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

    @Autowired
    private RolePermissionService rolePermissionService;

    @Override
    public List<Permission> queryAllPermission() {
        List<Permission> allPermissionList = this.list();
        return PermissionHelper.buildPermission(allPermissionList);
    }

    @Override
    public void removeChildById(Long id) {
        //创建菜单集合idList
        List<Long> idList = new ArrayList<>();
        //递归找子菜单
        this.getAllPermissionId(id, idList);
        //添加当前菜单id
        idList.add(id);
        //删除
        baseMapper.deleteBatchIds(idList);
    }

    @Override
    public List<Permission> findByRoleId(Long roleId) {
        //获取树形结构的菜单表
        List<Permission> buildPermissions = PermissionHelper.buildPermission(this.list());

        LambdaQueryWrapper<RolePermission> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(RolePermission::getRoleId, roleId);

        List<Long> existPermission = rolePermissionService.list(wrapper)
                .stream()
                .map(RolePermission::getPermissionId)
                .collect(Collectors.toList());

        for (Permission permission : buildPermissions) {
            if (permission.getPid() == 0) {
                if (existPermission.contains(permission.getId())) {
                    permission.setSelect(true);
                }
                this.setChildrenSelect(permission.getChildren(), existPermission);

            }
        }
        return buildPermissions;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveRoleAndPermission(Long roleId, Long[] permissionId) {
        //先根据角色id删除先分配的菜单信息
        LambdaQueryWrapper<RolePermission> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(RolePermission::getRoleId, roleId);
        //移除
        rolePermissionService.remove(wrapper);
        //重新为角色分配菜单
        List<RolePermission> rolePermissionList = new ArrayList<>();
        for (Long pid : permissionId) {
            RolePermission rolePermission = new RolePermission();
            rolePermission.setRoleId(roleId);
            rolePermission.setPermissionId(pid);
            rolePermissionList.add(rolePermission);
        }
        //重新插入信息
        rolePermissionService.saveBatch(rolePermissionList);
    }

    private void setChildrenSelect(List<Permission> children, List<Long> existPermission) {
        for (Permission permission : children) {
            if (existPermission.contains(permission.getId())) {
                permission.setSelect(true);
            }
            this.setChildrenSelect(permission.getChildren(), existPermission);
        }
    }

    private void getAllPermissionId(Long id, List<Long> idList) {
        //根据pid查询子菜单
        LambdaQueryWrapper<Permission> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Permission::getPid, id);

        List<Permission> childrenList = baseMapper.selectList(wrapper);
        childrenList.forEach(item -> {
            Long currentId = item.getId();
            //封装菜单到idList
            idList.add(currentId);
            //递归查询
            this.getAllPermissionId(currentId, idList);
        });
    }
}
