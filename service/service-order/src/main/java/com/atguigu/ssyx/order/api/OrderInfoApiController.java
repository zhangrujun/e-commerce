package com.atguigu.ssyx.order.api;


import com.atguigu.ssyx.model.order.OrderInfo;
import com.atguigu.ssyx.order.service.OrderInfoService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "订单管理内部接口")
@RestController
@RequestMapping("/api/order")
public class OrderInfoApiController {

    @Autowired
    private OrderInfoService orderInfoService;

    @GetMapping("/inner/getOrderInfo/{orderNo}")
    public OrderInfo getOrderInfoByOrderNo(@PathVariable String orderNo) {
        return orderInfoService.getOrderInfoByOrderNo(orderNo);
    }

}
