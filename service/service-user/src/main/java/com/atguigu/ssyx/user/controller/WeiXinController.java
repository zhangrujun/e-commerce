package com.atguigu.ssyx.user.controller;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.ssyx.common.Result.Result;
import com.atguigu.ssyx.common.Result.ResultCodeEnum;
import com.atguigu.ssyx.common.auth.AuthContextHolder;
import com.atguigu.ssyx.common.constant.RedisConstant;
import com.atguigu.ssyx.common.exception.CustomException;
import com.atguigu.ssyx.enums.UserType;
import com.atguigu.ssyx.model.user.User;
import com.atguigu.ssyx.user.service.LeaderService;
import com.atguigu.ssyx.user.service.UserService;
import com.atguigu.ssyx.user.utils.ConstantPropertiesUtils;
import com.atguigu.ssyx.user.utils.HttpClientUtils;
import com.atguigu.ssyx.utils.JwtHelper;
import com.atguigu.ssyx.vo.user.LeaderAddressVo;
import com.atguigu.ssyx.vo.user.UserLoginVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Api(tags = "微信登录相关接口")
@RestController
@RequestMapping("/api/user/weixin")
@Slf4j
public class WeiXinController {

    @Autowired
    private UserService userService;

    @Autowired
    private LeaderService leaderService;

    @Autowired
    private RedisTemplate redisTemplate;

    @ApiOperation(value = "微信登录获取openid(小程序)")
    @GetMapping("/wxLogin/{code}")
    public Result wxLogin(@PathVariable String code) {
        //判断是否有code值
        if (code == null) {
            throw new CustomException(ResultCodeEnum.ILLEGAL_CALLBACK_REQUEST_ERROR);
        }
        //输出临时凭证值
        log.info("code凭证值:{}", code);
        //使用code和appid以及appSecret换取access_token
        StringBuilder baseAccessTokenUrl = new StringBuilder()
                .append("https://api.weixin.qq.com/sns/jscode2session")
                .append("?appid=%s")
                .append("&secret=%s")
                .append("&js_code=%s")
                .append("&grant_type=authorization_code");
        //拼接占位符
        String url = String.format(
                baseAccessTokenUrl.toString(),
                ConstantPropertiesUtils.WX_OPEN_APP_ID,
                ConstantPropertiesUtils.WX_OPEN_APP_SECRET,
                code
        );
        //创建调用结果
        String jsonResult;
        //调用HttpClient
        try {
            jsonResult = HttpClientUtils.get(url);
        } catch (Exception e) {
            throw new CustomException(ResultCodeEnum.FETCH_ACCESS_TOKEN_FAIL);
        }
        //将结果转成Java对象
        JSONObject jsonObject = JSONObject.parseObject(jsonResult);
        //获取session_key和openid
        String openid = jsonObject.getString("openid");
        //根据openid查询是否有
        User user = userService.getByOpenid(openid);
        log.info("查询用户信息:{}", user);
        if (user == null) {
            //如果没有查到用户信息,那么调用微信个人信息获取的接口
            user = new User();
            user.setOpenId(openid);
            user.setNickName(openid);
            user.setPhotoUrl("");
            user.setUserType(UserType.USER);
            user.setIsNew(0);
            userService.save(user);
        }
        LeaderAddressVo leaderAddressVo = leaderService.getLeaderAddressVoByUserId(user.getId());
        Map<String, Object> map = new HashMap<>();
        map.put("user", user);
        map.put("leaderAddressVo", leaderAddressVo);

        String name = user.getNickName();
        String token = JwtHelper.createToken(user.getId(), name);
        map.put("token", token);
        UserLoginVo userLoginVo = userService.getUserLoginVo(user.getId());
        //设置仓库id
        userLoginVo.setWareId(leaderAddressVo.getWareId());
        log.info("获取登录用户信息:{}", userLoginVo);
        redisTemplate.opsForValue().set(
                RedisConstant.USER_LOGIN_KEY_PREFIX + user.getId(),
                userLoginVo,
                RedisConstant.USER_KEY_TIMEOUT,
                TimeUnit.DAYS
        );
        return Result.ok(map);
    }

    @PostMapping("/auth/updateUser")
    @ApiOperation(value = "更新用户昵称与头像")
    public Result updateUser(@RequestBody User user) {
        User queryUser = userService.getById(AuthContextHolder.getUserId());
        //把昵称更新为微信用户
        queryUser.setNickName(user.getNickName().replaceAll("[ue000-uefff]", "*"));
        queryUser.setPhotoUrl(user.getPhotoUrl());
        userService.updateById(queryUser);
        return Result.ok();
    }
}
