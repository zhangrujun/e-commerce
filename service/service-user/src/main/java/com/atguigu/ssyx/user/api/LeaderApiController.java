package com.atguigu.ssyx.user.api;

import com.atguigu.ssyx.user.service.LeaderService;
import com.atguigu.ssyx.vo.user.LeaderAddressVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "团长管理内部接口")
@RestController
@RequestMapping("/api/user/leader")
public class LeaderApiController {

    @Autowired
    private LeaderService leaderService;

    @ApiOperation("提货点地址信息")
    @GetMapping("/inner/getUserAddressByUserId/{userId}")
    public LeaderAddressVo getLeaderAddressVoByUserId(@PathVariable Long userId) {
        return leaderService.getLeaderAddressVoByUserId(userId);
    }
}
