package com.atguigu.ssyx.user.service;

import com.atguigu.ssyx.model.user.Leader;
import com.atguigu.ssyx.vo.user.LeaderAddressVo;
import com.baomidou.mybatisplus.extension.service.IService;

public interface LeaderService extends IService<Leader> {
    LeaderAddressVo getLeaderAddressVoByUserId(Long userId);
}
