package com.atguigu.ssyx.payment.controller;

import com.atguigu.ssyx.common.Result.Result;
import com.atguigu.ssyx.enums.PaymentType;
import com.atguigu.ssyx.payment.service.PaymentInfoService;
import com.atguigu.ssyx.payment.service.WeiXinService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Api(tags = "微信支付接口")
@RestController
@RequestMapping("/api/payment/weixin")
public class WeiXinController {

    @Autowired
    private WeiXinService weiXinPayService;

    @Autowired
    private PaymentInfoService paymentInfoService;

    @ApiOperation(value = "创建Jsapi接口")
    @GetMapping("/createJsapi/{orderNo}")
    public Result<Map<String, Object>> createJsapi(@PathVariable String orderNo) {
        Map<String, Object> map = weiXinPayService.createJsapi(orderNo);
        return Result.ok(map);
    }

    @ApiOperation(value = "查询支付状态")
    @GetMapping("/queryPayStatus/{orderNo}")
    public Result queryPayStatus(@PathVariable String orderNo) {
        //调用查询接口
        Map<String, String> resultMap = weiXinPayService.queryPayStatus(orderNo, PaymentType.WEIXIN.name());
        if (resultMap == null) {//出错
            return Result.fail("支付出错");
        }
        if ("SUCCESS".equals(resultMap.get("trade_state"))) {//如果成功
            //更改订单状态，处理支付结果
            String outTradeNo = resultMap.get("out_trade_no");
            paymentInfoService.paySuccess(outTradeNo, resultMap);
            return Result.ok("支付成功");
        }
        return Result.ok("支付中");
    }

}
