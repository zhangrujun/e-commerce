package com.atguigu.ssyx.sys.controller;


import com.atguigu.ssyx.common.Result.Result;
import com.atguigu.ssyx.model.sys.RegionWare;
import com.atguigu.ssyx.sys.service.RegionWareService;
import com.atguigu.ssyx.vo.sys.RegionWareQueryVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 城市仓库关联表 前端控制器
 * </p>
 *
 * @author atguigu
 * @since 2023-09-23
 */

@Api(tags = "区域仓库相关接口")
@RestController
@RequestMapping("/admin/sys/regionWare")
public class RegionWareController {

    @Autowired
    private RegionWareService regionWareService;

    /**
     * 查询开通已经的区域
     *
     * @param page
     * @param limit
     * @param regionWareQueryVo
     * @return
     */
    @ApiOperation("查询开通已经的区域")
    @GetMapping("/{page}/{limit}")
    public Result<IPage<RegionWare>> list(@PathVariable Long page,
                                          @PathVariable Long limit,
                                          RegionWareQueryVo regionWareQueryVo) {
        Page<RegionWare> pageParam = new Page<>(page, limit);
        IPage<RegionWare> result = regionWareService.selectPageRegionWare(pageParam, regionWareQueryVo);
        return Result.ok(result);
    }

    /**
     * 开通区域
     *
     * @param regionWare
     * @return
     */
    @ApiOperation("开通区域")
    @PostMapping("/save")
    public Result addRegionWare(@RequestBody RegionWare regionWare) {
        regionWareService.saveRegionWare(regionWare);
        return Result.ok();
    }

    /**
     * 删除开通区域
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "删除")
    @DeleteMapping("remove/{id}")
    public Result remove(@PathVariable Long id) {
        regionWareService.removeById(id);
        return Result.ok();
    }

    /**
     * 取消开通区域
     *
     * @param id
     * @param status
     * @return
     */
    @ApiOperation(value = "取消开通区域")
    @PostMapping("updateStatus/{id}/{status}")
    public Result updateStatus(@PathVariable Long id, @PathVariable Integer status) {
        regionWareService.updateStatus(id, status);
        return Result.ok();
    }
}

