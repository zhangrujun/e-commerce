package com.atguigu.ssyx.sys.controller;


import com.atguigu.ssyx.common.Result.Result;
import com.atguigu.ssyx.model.sys.Region;
import com.atguigu.ssyx.sys.service.RegionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 地区表 前端控制器
 * </p>
 *
 * @author atguigu
 * @since 2023-09-23
 */
@Api(tags = "区域相关接口")
@RestController
@RequestMapping("/admin/sys/region")
public class RegionController {

    @Autowired
    private RegionService regionService;

    /**
     * 根据关键字查询所有区域
     *
     * @param keyword
     * @return
     */
    @ApiOperation("根据关键字查询所有区域")
    @GetMapping("findRegionByKeyword/{keyword}")
    public Result<List<Region>> findRegionByKeyword(@PathVariable String keyword) {
        List<Region> list = regionService.getRegionByKeyword(keyword);
        return Result.ok(list);
    }
}

