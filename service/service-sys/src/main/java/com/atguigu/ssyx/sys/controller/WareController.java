package com.atguigu.ssyx.sys.controller;


import com.atguigu.ssyx.common.Result.Result;
import com.atguigu.ssyx.model.sys.Ware;
import com.atguigu.ssyx.sys.service.WareService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 仓库表 前端控制器
 * </p>
 *
 * @author atguigu
 * @since 2023-09-23
 */
@Api(tags = "仓库相关接口")
@RestController
@RequestMapping("/admin/sys/ware")
@Slf4j
public class WareController {

    @Autowired
    private WareService wareService;

    /**
     * 查询所有的仓库信息
     *
     * @return
     */
    @ApiOperation("查询所有的仓库信息")
    @GetMapping("/findAllList")
    public Result<List<Ware>> findAllList() {
        List<Ware> list = wareService.list();
        log.info("结果:{}",list);
        return Result.ok(list);
    }
}

