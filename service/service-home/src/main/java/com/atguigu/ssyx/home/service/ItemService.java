package com.atguigu.ssyx.home.service;

import java.util.Map;

public interface ItemService {

    Map<String, Object> getItem(Long skuId, Long userId);
}
