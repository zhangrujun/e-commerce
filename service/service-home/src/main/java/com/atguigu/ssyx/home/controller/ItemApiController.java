package com.atguigu.ssyx.home.controller;

import com.atguigu.ssyx.common.Result.Result;
import com.atguigu.ssyx.common.auth.AuthContextHolder;
import com.atguigu.ssyx.home.service.ItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Api(tags = "商品详情相关接口")
@RestController
@RequestMapping("/api/home")
public class ItemApiController {

    @Autowired
    private ItemService itemService;

    @ApiOperation("获取sku详细信息")
    @GetMapping("/item/{id}")
    public Result<Map<String, Object>> getItem(@PathVariable Long id) {
        Long userId = AuthContextHolder.getUserId();
        Map<String, Object> result = itemService.getItem(id, userId);
        return Result.ok(result);
    }
}
