package com.atguigu.ssyx.home.controller;

import com.atguigu.ssyx.common.Result.Result;
import com.atguigu.ssyx.common.auth.AuthContextHolder;
import com.atguigu.ssyx.home.service.HomeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Api(tags = "主页相关接口")
@RestController
@RequestMapping("/api/home")
public class HomeController {

    @Autowired
    private HomeService homeService;

    @ApiOperation("获取首页数据")
    @GetMapping("/index")
    public Result<Map<String, Object>> index() {
        Long userId = AuthContextHolder.getUserId();
        return Result.ok(homeService.home(userId));
    }
}
