package com.atguigu.ssyx.home.service;

import java.util.Map;

public interface HomeService {

    Map<String, Object> home(Long userId);
}
