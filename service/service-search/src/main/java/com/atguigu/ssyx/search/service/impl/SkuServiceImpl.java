package com.atguigu.ssyx.search.service.impl;

import com.alibaba.fastjson.JSON;
import com.atguigu.ssyx.client.activity.ActivityFeignClient;
import com.atguigu.ssyx.client.product.ProductFeignClient;
import com.atguigu.ssyx.common.Result.ResultCodeEnum;
import com.atguigu.ssyx.common.auth.AuthContextHolder;
import com.atguigu.ssyx.common.constant.RedisConstant;
import com.atguigu.ssyx.common.exception.CustomException;
import com.atguigu.ssyx.enums.SkuType;
import com.atguigu.ssyx.model.product.Category;
import com.atguigu.ssyx.model.product.SkuInfo;
import com.atguigu.ssyx.model.search.SkuEs;
import com.atguigu.ssyx.search.repository.SkuRepository;
import com.atguigu.ssyx.search.service.SkuService;
import com.atguigu.ssyx.vo.search.SkuEsQueryVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SkuServiceImpl implements SkuService {

    @Autowired
    private ProductFeignClient productFeignClient;

    @Autowired
    private SkuRepository skuEsRepository;

    @Autowired
    private ActivityFeignClient activityFeignClient;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public void upperSku(Long skuId) {
        log.info("upperSku{}", skuId);
        SkuEs skuEs = new SkuEs();

        //查询sku信息
        SkuInfo skuInfo = productFeignClient.getSkuInfo(skuId);
        if (skuInfo == null) {
            throw new CustomException(ResultCodeEnum.ILLEGAL_REQUEST);
        }

        // 查询分类
        Category category = productFeignClient.getCategory(skuInfo.getCategoryId());
        if (category != null) {
            skuEs.setCategoryId(category.getId());
            skuEs.setCategoryName(category.getName());
        }
        skuEs.setId(skuInfo.getId());
        skuEs.setKeyword(skuInfo.getSkuName() + "," + skuEs.getCategoryName());
        skuEs.setWareId(skuInfo.getWareId());
        skuEs.setIsNewPerson(skuInfo.getIsNewPerson());
        skuEs.setImgUrl(skuInfo.getImgUrl());
        skuEs.setTitle(skuInfo.getSkuName());
        if (Objects.equals(skuInfo.getSkuType(), SkuType.COMMON.getCode())) {
            skuEs.setSkuType(0);
            skuEs.setPrice(skuInfo.getPrice().doubleValue());
            skuEs.setStock(skuInfo.getStock());
            skuEs.setSale(skuInfo.getSale());
            skuEs.setPerLimit(skuInfo.getPerLimit());
        } else {
            //TODO 待完善-秒杀商品
        }
        SkuEs save = skuEsRepository.save(skuEs);
        log.info("upperSku：{}", JSON.toJSONString(save));
    }

    @Override
    public void lowerSku(Long skuId) {
        log.info("lowerSku：{}", skuId);
        skuEsRepository.deleteById(skuId);
    }

    @Override
    public List<SkuEs> findHotSkuList() {
        //0代表第一页
        Pageable pageable = PageRequest.of(0, 10);
        Page<SkuEs> pageModel = skuEsRepository.findByOrderByHotScoreDesc(pageable);
        return pageModel.getContent();
    }

    @Override
    public Page<SkuEs> search(Pageable pageable, SkuEsQueryVo searchParamVo) {
        //设置仓库id
        searchParamVo.setWareId(AuthContextHolder.getWareId());
        //获取keyword
        String keyword = searchParamVo.getKeyword();
        //结果
        Page<SkuEs> page;
        //判断keyword是否为空
        if (StringUtils.isEmpty(keyword)) {
            //根据分类id和仓库id查询
            page = skuEsRepository.findByCategoryIdAndWareId(searchParamVo.getCategoryId(), searchParamVo.getWareId(), pageable);
        } else {
            //根据仓库id和keyword查询
            page = skuEsRepository.findByWareIdAndKeyword(searchParamVo.getWareId(), keyword, pageable);
        }
        //查询商品参加的优惠活动
        List<SkuEs> skuEsList = page.getContent();
        if (!skuEsList.isEmpty()) {
            //获取skuEsId(商品id)
            List<Long> skuEsIdList = skuEsList.stream()
                    .map(SkuEs::getId)
                    .collect(Collectors.toList());
            //获取所有活动规则
            Map<Long, List<String>> ruleListMap = activityFeignClient.findActivity(skuEsIdList);
            skuEsList.forEach(
                    item -> item.setRuleList(ruleListMap.get(item.getId()))
            );
        }
        return page;
    }

    @Override
    public void incrHotScore(Long skuId) {
        // 保存数据
        Double hotScore = redisTemplate.opsForZSet().incrementScore(RedisConstant.HOT_KEY, "skuId:" + skuId, 1);
        if (hotScore % 10 == 0) {
            // 更新es
            Optional<SkuEs> optional = skuEsRepository.findById(skuId);
            SkuEs skuEs = optional.get();
            skuEs.setHotScore(Math.round(hotScore));
            skuEsRepository.save(skuEs);
        }
    }
}
