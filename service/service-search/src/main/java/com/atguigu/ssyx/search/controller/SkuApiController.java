package com.atguigu.ssyx.search.controller;

import com.atguigu.ssyx.common.Result.Result;
import com.atguigu.ssyx.model.search.SkuEs;
import com.atguigu.ssyx.search.service.SkuService;
import com.atguigu.ssyx.vo.search.SkuEsQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "商品搜索列表接口")
@RestController
@RequestMapping("/api/search/sku")
@Slf4j
public class SkuApiController {

    @Autowired
    private SkuService skuService;

    @ApiOperation("上架商品")
    @GetMapping("/inner/upperSku/{skuId}")
    public Result upperGoods(@PathVariable Long skuId) {
        skuService.upperSku(skuId);
        return Result.ok();
    }

    @ApiOperation("下架商品")
    @GetMapping("/inner/lowerSku/{skuId}")
    public Result lowerGoods(@PathVariable Long skuId) {
        skuService.lowerSku(skuId);
        return Result.ok();
    }

    @ApiOperation("获取爆品商品")
    @GetMapping("/inner/findHotSkuList")
    public List<SkuEs> findHotSkuList() {
        return skuService.findHotSkuList();
    }

    @ApiOperation("搜索商品")
    @GetMapping("/{page}/{limit}")
    public Result list(@PathVariable("page") Integer page,
                       @PathVariable("limit") Integer limit,
                       SkuEsQueryVo searchParamVo) {
        log.info("查询条件,{},{}", page, limit);
        Pageable pageable = PageRequest.of(page - 1, limit);
        Page<SkuEs> pageModel = skuService.search(pageable, searchParamVo);
        log.info("结果:{}", pageModel.getContent());
        return Result.ok(pageModel);
    }

    @ApiOperation(value = "更新商品incrHotScore")
    @GetMapping("inner/incrHotScore/{skuId}")
    public Boolean incrHotScore(@PathVariable("skuId") Long skuId) {
        // 调用服务层
        skuService.incrHotScore(skuId);
        return true;
    }
}
