package com.atguigu.ssyx.activity.mapper;

import com.atguigu.ssyx.model.activity.CouponRange;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 优惠券范围表 Mapper 接口
 * </p>
 *
 * @author L0VE、SsJuN
 * @since 2023-10-25
 */
@Mapper
public interface CouponRangeMapper extends BaseMapper<CouponRange> {

}
