package com.atguigu.ssyx.activity.mapper;

import com.atguigu.ssyx.model.activity.ActivityRule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 优惠规则 Mapper 接口
 * </p>
 *
 * @author L0VE、SsJuN
 * @since 2023-10-25
 */
@Mapper
public interface ActivityRuleMapper extends BaseMapper<ActivityRule> {

}
