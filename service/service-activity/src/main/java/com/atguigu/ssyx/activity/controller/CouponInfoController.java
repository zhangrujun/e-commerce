package com.atguigu.ssyx.activity.controller;


import com.atguigu.ssyx.activity.service.CouponInfoService;
import com.atguigu.ssyx.common.Result.Result;
import com.atguigu.ssyx.model.activity.CouponInfo;
import com.atguigu.ssyx.vo.activity.CouponRuleVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 优惠券信息 前端控制器
 * </p>
 *
 * @author L0VE、SsJuN
 * @since 2023-10-25
 */
@Api(tags = "优惠券相关接口")
@RestController
@RequestMapping("/admin/activity/couponInfo")
public class CouponInfoController {

    @Autowired
    private CouponInfoService couponInfoService;

    @ApiOperation("分页查询")
    @GetMapping("/{page}/{limit}")
    public Result<IPage<CouponInfo>> list(@PathVariable Long page,
                                          @PathVariable Long limit) {
        Page<CouponInfo> pageParam = new Page<>(page, limit);
        IPage<CouponInfo> result = couponInfoService.selectPage(pageParam);
        return Result.ok(result);
    }

    @ApiOperation("根据id查询")
    @GetMapping("/get/{id}")
    public Result<CouponInfo> getById(@PathVariable Long id) {
        CouponInfo couponInfo = couponInfoService.getById(id);
        return Result.ok(couponInfo);
    }

    @ApiOperation("添加优惠券")
    @GetMapping("/save")
    public Result save(@RequestBody CouponInfo couponInfo) {
        couponInfoService.save(couponInfo);
        return Result.ok();
    }

    @ApiOperation("修改优惠券信息")
    @PutMapping("/update")
    public Result updateById(@RequestBody CouponInfo couponInfo) {
        couponInfoService.updateById(couponInfo);
        return Result.ok();
    }

    @ApiOperation("获取优惠券信息")
    @GetMapping("/findCouponRuleList/{id}")
    public Result<Map<String, Object>> findCouponRuleList(@PathVariable Long id) {
        Map<String, Object> result = couponInfoService.findCouponRuleList(id);
        return Result.ok(result);
    }

    @ApiOperation("新增活动")
    @PostMapping("/saveCouponRule")
    public Result saveCouponRule(@RequestBody CouponRuleVo couponRuleVo) {
        couponInfoService.saveCouponRule(couponRuleVo);
        return Result.ok();
    }
}

