package com.atguigu.ssyx.activity.mapper;

import com.atguigu.ssyx.model.activity.ActivitySku;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 活动参与商品 Mapper 接口
 * </p>
 *
 * @author L0VE、SsJuN
 * @since 2023-10-25
 */
@Mapper
public interface ActivitySkuMapper extends BaseMapper<ActivitySku> {
    List<Long> selectExistSkuId(@Param("skuIdList") List<Long> skuIdList);
}
